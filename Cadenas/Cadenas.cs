using System;
using System.Text;
namespace MisCadenas{
class Cadena{
	public static string EliminaEspacios(string cad){
		string aux = "";
		bool primerBlanco = true;
		string cadSinEspaciosPF = cad.Trim();
		for (int i = 0; i < cadSinEspaciosPF.Length; ++i) {
			if (cadSinEspaciosPF[i] != ' '){
				aux += cadSinEspaciosPF[i];
				primerBlanco = true;
			}
			else if (cadSinEspaciosPF[i] == ' ' &&
			         primerBlanco){
			         	aux += ' ';
			         	primerBlanco = false; 
			         }
				
		}
		return aux;
	}
	public static int CuentaPalabras(string cad){
		int contPalabras = 0;
		bool haComenzadoPalabra = false;
		for (int i = 0; i < cad.Length; ++i){
			if (cad[i] != ' ' && !haComenzadoPalabra){
				contPalabras++;
				haComenzadoPalabra = true;	
			}
			else if (cad[i] == ' ')	
				haComenzadoPalabra = false;
					
		}
		return contPalabras;	
	}
	
	public static string Invierte(string cad) {
		string aux = "";
		for (int i = cad.Length - 1; i >= 0; --i){
			aux += cad[i];
		}
		return aux;
		
	}
	
	public static int CuentaCadenas(string cad, string cadBuscar){
		int contCad = 0;
		int i = 0;
		int posEncuentra;
		
		if (cadBuscar.Length > cad.Length)
				return contCad;
		
		posEncuentra = cad.IndexOf(cadBuscar, i); 
		while (posEncuentra != -1 && i < cad.Length) {
			contCad++;
			i = posEncuentra + 1;
			if (i < cad.Length)
				posEncuentra = cad.IndexOf(cadBuscar, i);
		}
		return contCad;
	}
	
	public static string AMayusculas(string cad){
		StringBuilder aux  = new StringBuilder(cad.ToUpper());
		for (int i = 0; i < aux.Length; ++i) {
				if (aux[i] == '�')
						aux[i] = '�';
		}
		return aux.ToString();
	}
	
	public static void Clasifica(string[] cad){
		string aux;
		for (int i = 0; i < cad.GetLength(0); ++i){
			for (int j = i + 1; j < cad.GetLength(0); ++j){
				if (cad[i].CompareTo(cad[j]) >  0)  {
					aux = cad[i];
					cad[i] = cad[j];
					cad[j] = aux;
				}
			}
		}
	}
	public static string Rota(string cad, bool rotarDerecha) {
		StringBuilder aux = new StringBuilder(cad);
		if (rotarDerecha) {
			char carFinal = aux[aux.Length - 1];
			for (int i = aux.Length - 2; i >= 0; --i) {
				aux[i+1] = aux[i];
			}
			aux[0] = carFinal;
		}
		else {
			char carPrincipio = aux[0];
			for (int i = 1; i < aux.Length; ++i){
				aux[i-1]=aux[i];
			}
			aux[aux.Length - 1] = carPrincipio;
		}
		cad = aux.ToString();
		return cad;
	}
}
}
