using System;
using MisCadenas;
class PruebaCadenas{
	public static void Main(string[] args) {
		string cad = "   Hoy   es   martes  ";
		Console.WriteLine("Cadena sin tratar:");
		Console.WriteLine(cad);
		Console.WriteLine("Cadena sin espacios:");
		string cadSinEspacios = Cadena.EliminaEspacios(cad);
		Console.WriteLine(cadSinEspacios);
		Console.WriteLine("'es' aparece {0} veces",Cadena.CuentaCadenas(cadSinEspacios, "es"));
		Console.WriteLine("Cadena en mayusculas");
		Console.WriteLine(Cadena.AMayusculas(cadSinEspacios));
		Console.WriteLine("Numero de palabras:{0}", Cadena.CuentaPalabras(cad));
		Console.WriteLine("Cadena invertida:");
		Console.WriteLine(Cadena.Invierte(cadSinEspacios));
		Console.WriteLine("Rotando hacia la derecha");
		for (int i = 0; i < 3; ++i){
				cadSinEspacios=Cadena.Rota(cadSinEspacios, true);
				Console.WriteLine(cadSinEspacios);
		}
		
		Console.WriteLine("Rotando hacia la izquierda");
		for (int i = 0; i < 3; ++i){
				cadSinEspacios=Cadena.Rota(cadSinEspacios, false);
				Console.WriteLine(cadSinEspacios);
		}
		
		string[] tabla = {"ma�ana", "hoy", "ayer", "pasado", "presente"};
		Console.WriteLine("Vector sin clasificar");
		for (int i = 0; i < tabla.GetLength(0); ++i){
			Console.Write(tabla[i] + " ");
		}
		Console.WriteLine();
		Console.WriteLine("Vector clasificado");
		Cadena.Clasifica(tabla);
		for (int i = 0; i < tabla.GetLength(0); ++i){
			Console.Write(tabla[i] + " ");
		}	
		
		
		Console.ReadLine();
		
	}
}

